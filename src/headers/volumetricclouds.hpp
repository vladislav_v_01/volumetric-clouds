#ifndef VOLUMETRICCLOUDS_HPP
#define VOLUMETRICCLOUDS_HPP

#include <memory>

#include "GLFW/glfw3.h"

#include "camera.hpp"

#include "shader.hpp"

class VolumetricClouds
{
public:
    VolumetricClouds();

    ~VolumetricClouds();

    void Initialize();

    void Update();

    int Run();

private:
    void InitializeBase();

    void OnWindowResize(const int width, const int height);
    void OnKeyPress(const int keyCode);
    void OnKeyRelease(const int keyCode);
    void OnMouseMove(const float xOffset, const float yOffset);
    void OnMouseScroll(const float verticalOffset);


    void GLFWFramebufferSizeCallback(GLFWwindow* window,
                                     const int   width,
                                     const int   height);
    void GLFWMouseCallback(GLFWwindow*  window,
                           const double xPos,
                           const double yPos);
    void GLFWScrollCallback(GLFWwindow*  window,
                            const double xOffset,
                            const double yOffset);
    void GLFWKeyCallback(GLFWwindow* widnow,
                         const int   key,
                         const int   scancode,
                         const int   action,
                         const int   mods);

    static void GLFWFramebufferSizeCallbackHelper(GLFWwindow* window,
                                                  int         width,
                                                  int         height);
    static void GLFWMouseCallbackHelper(GLFWwindow* window,
                                        double      x_pos,
                                        double      y_pos);
    static void GLFWScrollCallbackHelper(GLFWwindow* window,
                                         double      x_offset,
                                         double      y_offset);
    static void GLFWKeyCallbackHelper(
        GLFWwindow* window, int key, int scancode, int action, int mods);


private:
    GLFWwindow *m_Window = nullptr;
    float       m_DeltaTime = 0.0f;

    bool m_ShowDebugGui = true;
    bool m_Changed      = true;

    unsigned int m_QuadVAO;
    unsigned int m_QuadVBO;

    std::unique_ptr<Camera> m_UserCamera = nullptr;

    std::unique_ptr<Shader> m_SkyShader = nullptr;

    float m_AngleOfSun = 70.0;
    glm::vec3 m_SunDirection = glm::vec3(0.0f, 0.507124f, -0.861873f);
    float m_EarthRadius = 6371e3;
    float m_AtmodhereRadius = 6471e3;
    float m_Hr = 8000.0f;
    float m_Hm = 1200.0f;
    glm::vec3 m_BetaR = glm::vec3(5.8e-6f, 13.5e-6f, 33.1e-6f);
    glm::vec3 m_BetaM = glm::vec3(2e-7f);
    int m_NumSamples = 16;
    int m_NumSamplesLight = 8;

    float m_LastX = 0.f;
    float m_LastY = 0.f;


};

#endif // VOLUMETRICCLOUDS_HPP
