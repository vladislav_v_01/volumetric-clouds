#ifndef CAMERA_H
#define CAMERA_H

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

class Camera
{
public:
    Camera(const glm::vec3& position_,
           float            near_              = 0.1f,
           float            far_               = 1000.f,
           float            movement_speed_    = 2.5f,
           float            mouse_sensitivity_ = 0.1f);

    inline glm::mat4 GetProjViewMatrix(float aspect_ratio) const
    {
        return glm::perspective(glm::radians(FovY), aspect_ratio, ZNear, ZFar) *
               glm::lookAt(Position, Position + Front, Up);
    }
    inline glm::mat4 GetProjMatrix(float aspect_ratio) const
    {
        return glm::perspective(glm::radians(FovY), aspect_ratio, ZNear, ZFar);
    }

    inline glm::mat4 GetViewMatrix()
    {
        UpdateCameraVectors();
        return glm::lookAt(Position, Position + Front, Up);
    }

    // Todo: This keyboard movement is kind of choppy fix it ffs
    void ProcessKeyboard(int key_code, float delta_time);
    void ProcessMouseMove(float x_offset, float y_offset);
    void ProcessMouseScroll(float vertical_offset);

    glm::vec3 Position;

private:
    void UpdateCameraVectors();

    glm::vec3 Front; // Z
    glm::vec3 Right; // X
    glm::vec3 Up;    // Y

    const glm::vec3 WorldUp = glm::vec3(0.f, 1.f, 0.f);
    float           MovementSpeed;
    float           MouseSensitivity;

    float Pitch = 0.f;   // X
    float Yaw   = -90.f; // Y
    float FovY  = 60.f;

    float ZNear;
    float ZFar;
};

#endif
