/*******************************************************************
** This code is part of Breakout.
**
** Breakout is free software: you can redistribute it and/or modify
** it under the terms of the CC BY 4.0 license as published by
** Creative Commons, either version 4 of the License, or (at your
** option) any later version.
******************************************************************/
#ifndef TEXTURE_H
#define TEXTURE_H

#include "glad.h"

#include "property.h"

// Texture2D is able to store and configure a texture in OpenGL.
// It also hosts utility functions for easy management.
class Texture2D
{

    PROPERTY(GLuint, ID);
    PROPERTY(unsigned int, Width);
    PROPERTY(unsigned int, Height);
    PROPERTY(GLint, InternalFormat);

public:
    Texture2D(unsigned int  width_,
              unsigned int  height_,
              GLint         internal_format_,
              GLenum        format,
              GLenum        type,
              GLint         min_filter = GL_NEAREST,
              GLint         max_filter = GL_NEAREST,
              GLint         wrap_r     = GL_CLAMP_TO_BORDER,
              GLint         wrap_s     = GL_CLAMP_TO_BORDER,
              const GLvoid* data       = 0);

    ~Texture2D();

    static Texture2D* CreateFromFile(const char* path,
                                     bool        flip_vertically = false);

    inline void Bind(const unsigned int unit = 0) const
    {
        glActiveTexture(GL_TEXTURE0 + unit);
        glBindTexture(GL_TEXTURE_2D, m_ID);
    }

    inline void Unbind() const { glBindTexture(GL_TEXTURE_2D, 0); }

    void SetWrappingParams(GLint wrap_r, GLint wrap_s);

    void BindImage(GLuint unit, GLenum access, GLenum format) const;
};

#endif
