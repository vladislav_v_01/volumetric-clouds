#ifndef APPSETTINGS_HPP
#define APPSETTINGS_HPP

#include "glad.h"

#include <string>
#include <iostream>

#include "property.h"


class AppSettings
{
    PROPERTY_DEFAULT(unsigned int, Width, 640);
    PROPERTY_DEFAULT(unsigned int, Height, 480);
    PROPERTY_DEFAULT(std::string, WindowTitle, "Clouds");
    PROPERTY_DEFAULT(bool, EnableCursor, true);
    PROPERTY_DEFAULT(bool, EnableDebugCallback, false);
public:
    static GLenum glCheckError_(const char* file, int line);
    #define glCheckError() glCheckError_(__FILE__, __LINE__)

    static void APIENTRY glDebugOutput(GLenum       source,
                                       GLenum       type,
                                       unsigned int id,
                                       GLenum       severity,
                                       GLsizei      length,
                                       const char*  message,
                                       const void*  userParam);


};

#endif // APPSETTINGS_HPP
