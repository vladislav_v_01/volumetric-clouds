#version 430 core
out vec4 FragColor;
in vec2 TexCoords;

uniform float aspectRatio;     // SCR_WIDTH / SCR_HEIGHT ?
uniform vec2  resolution;

uniform vec3  sunDirection; // The sun direction (normalized)
uniform float earthRadius;  // In the paper this is usually Rg or Re (radius ground,
                            // eart)
uniform float atmosphereRadius; // In the paper this is usually R or Ra (radius
                                // atmosphere)
uniform float Hr; // Thickness of the atmosphere if density was uniform (Hr)
uniform float Hm; // Same as above but for Mie scattering (Hm)

uniform vec3 betaR;
uniform vec3 betaM;

uniform int numSamples;
uniform int numSamplesLight;

#define M_PI 3.14159265358979323846f

bool solveQuadratic(const float a,const float b,const float c,inout float x1,inout float x2)
{
    if (b == 0)
    {
        // Handle special case where the the two vector ray.dir and V are
        // perpendicular with V = ray.orig - sphere.centre
        if (a == 0){
            return false;
        }
        x1 = 0;
        x2 = sqrt(-c / a);
        return true;
    }
    float discr = b * b - 4 * a * c;

    if (discr < 0){
        return false;
    }

    float q = (b < 0.0f) ? -0.5f * (b - sqrt(discr))
                        : -0.5f * (b + sqrt(discr));
    x1      = q / a;
    x2      = c / q;

    return true;
}

// A simple routine to compute the intersection of a ray with a sphere
bool raySphereIntersect(const vec3  orig,
                        const vec3  dir,
                        const float radius,
                        inout float   t0,
                        inout float   t1)
{
    // They ray dir is normalized so A = 1
    float A = dir.x * dir.x + dir.y * dir.y + dir.z * dir.z;
    float B = 2 * (dir.x * orig.x + dir.y * orig.y + dir.z * orig.z);
    float C =
        orig.x * orig.x + orig.y * orig.y + orig.z * orig.z - radius * radius;

    if (!solveQuadratic(A, B, C, t0, t1)){
        return false;
    }

    if (t0 > t1){
//        swap(t0, t1);
        float tmp = t0;
        t0 = t1;
        t1 = tmp;
    }

    return true;
}

vec3 computeIncidentLight(const vec3 orig,
                          const vec3 dir,
                          float      tmin,
                          float      tmax)
{
    float t0 = 0, t1 = 0;
    if (!(raySphereIntersect(orig, dir, atmosphereRadius, t0, t1) || t1 < 0))
        return vec3(0);
    if (t0 > tmin && t0 > 0)
        tmin = t0;
    if (t1 < tmax)
        tmax = t1;
//    int   numSamples      = 16;
//    int   numSamplesLight = 8;
    float segmentLength   = (tmax - tmin) / numSamples;
    float tCurrent        = tmin;
    vec3  sumR = vec3(0), sumM = vec3(0); // mie and rayleigh contribution
    float opticalDepthR = 0, opticalDepthM = 0;
    const float mu = dot(dir, sunDirection); // mu in the paper which is the cosine of the angle
                            // between the sun direction and the ray direction
    float phaseR = 3.0f / (16.0f * M_PI) * (1.0f + mu * mu);
    const float g      = 0.91f;
    float phaseM = 3.0f / (8.0f * M_PI) * ((1.0f - g * g) * (1.0f + mu * mu)) /
                   ((2.0f + g * g) *  pow((1.0f + g * g - 2.0f * g * mu), 1.5f));
    for (int i = 0; i < numSamples; ++i)
    {
        vec3 samplePosition =  orig + (tCurrent + segmentLength * 0.5f) * dir;
        float length = sqrt(samplePosition.x*samplePosition.x + samplePosition.y*samplePosition.y + samplePosition.z*samplePosition.z);
        float height         = length - earthRadius;
        // compute optical depth for light
        float hr = exp(-height / Hr) * segmentLength;
        float hm = exp(-height / Hm) * segmentLength;
        opticalDepthR += hr;
        opticalDepthM += hm;
        // light optical depth
        float t0Light = 0, t1Light = 0;
        raySphereIntersect(
            samplePosition, sunDirection, atmosphereRadius, t0Light, t1Light);
        float segmentLengthLight = t1Light / numSamplesLight, tCurrentLight = 0;
        float opticalDepthLightR = 0, opticalDepthLightM = 0;
        int j;
        for (j = 0; j < numSamplesLight; ++j)
        {
            vec3 samplePositionLight =
                samplePosition +
                (tCurrentLight + segmentLengthLight * 0.5f) * sunDirection;
            float samplePositionLightLength = sqrt(samplePositionLight.x*samplePositionLight.x + samplePositionLight.y*samplePositionLight.y + samplePositionLight.z*samplePositionLight.z);
            float heightLight = samplePositionLightLength - earthRadius;
            if (heightLight < 0)
                break;
            opticalDepthLightR += exp(-heightLight / Hr) * segmentLengthLight;
            opticalDepthLightM += exp(-heightLight / Hm) * segmentLengthLight;
            tCurrentLight += segmentLengthLight;
        }
        if (j == numSamplesLight)
        {
            vec3 tau = betaR * (opticalDepthR + opticalDepthLightR) +
                        betaM * 1.1f * (opticalDepthM + opticalDepthLightM);
            vec3 attenuation = vec3(exp(-tau.x), exp(-tau.y), exp(-tau.z));
            sumR += attenuation * hr;
            sumM += attenuation * hm;
        }
        tCurrent += segmentLength;
    }

    // We use a magic number here for the intensity of the sun (20). We will
    // make it more scientific in a future revision of this lesson/code
    return vec3((sumR * betaR * phaseR + sumM * betaM * phaseM) * 20);
}

void main()
{
    vec3  orig = vec3(0, earthRadius, 30000); // camera position
    float fov = 65;                           // 65
    float angle = tan(fov * M_PI / 180 * 0.5f);

    vec2 fragCoord = vec2(gl_FragCoord.xy);
    float rayx = (2 * gl_FragCoord.x / resolution.x - 1) * aspectRatio * angle;
    float rayy = (2 - ( resolution.y - gl_FragCoord.y) / resolution.y * 2) * angle;

    vec3 dir = vec3(rayx, rayy, -1.0f);
    float len2 = dir.x * dir.x + dir.y * dir.y  + dir.z * dir.z ;
    if(len2>0){
        float invLen = 1.0f / sqrt(len2);
        dir.x *= invLen;
        dir.y *= invLen;
        dir.z *= invLen;
    }


    float t0, t1, tMax = 3.40282e+38;
    if (raySphereIntersect(orig, dir, earthRadius, t0, t1) && t1 > 0){
        tMax = max(0.f, t0);
    }
    FragColor = vec4(computeIncidentLight(orig, dir, 0, tMax).xyz, 1.0f);
}
