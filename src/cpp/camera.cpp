#include "camera.hpp"

#include "GLFW/glfw3.h"

Camera::Camera(const glm::vec3& position_,
               float            near_,
               float            far_,
               float            movement_speed_,
               float            mouse_sensitivity_)
    : Position(position_)
    , MovementSpeed(movement_speed_)
    , MouseSensitivity(mouse_sensitivity_)
    , ZNear(near_)
    , ZFar(far_)
{
    UpdateCameraVectors();
}

void Camera::ProcessKeyboard(int key_code, float delta_time)
{
    float delta_distance = MovementSpeed * delta_time;

    if (key_code == GLFW_KEY_W)
        Position += Front * delta_distance;
    if (key_code == GLFW_KEY_S)
        Position -= Front * delta_distance;
    if (key_code == GLFW_KEY_A)
        Position -= Right * delta_distance;
    if (key_code == GLFW_KEY_D)
        Position += Right * delta_distance;
}

void Camera::ProcessMouseMove(float x_offset, float y_offset)
{
    Yaw += MouseSensitivity * x_offset;
    Pitch += MouseSensitivity * y_offset;

    Pitch = glm::clamp(Pitch, -89.f, 89.f);

    UpdateCameraVectors();
}

void Camera::ProcessMouseScroll(float vertical_offset)
{
    FovY -= vertical_offset;
    FovY = glm::clamp(FovY, 1.f, 45.f);
}

void Camera::UpdateCameraVectors()
{
    Front.x = glm::cos(glm::radians(Yaw)) * glm::cos(glm::radians(Pitch));
    Front.y = glm::sin(glm::radians(Pitch));
    Front.z = glm::sin(glm::radians(Yaw)) * glm::cos(glm::radians(Pitch));
    Front   = glm::normalize(Front);

    Right = glm::normalize(glm::cross(Front, WorldUp));
    Up    = glm::normalize(glm::cross(Right, Front));
}
