/*******************************************************************
** This code is part of Breakout.
**
** Breakout is free software: you can redistribute it and/or modify
** it under the terms of the CC BY 4.0 license as published by
** Creative Commons, either version 4 of the License, or (at your
** option) any later version.
******************************************************************/
#include <iostream>

#include "stb_image.h"

#include "texture.hpp"

Texture2D::Texture2D(unsigned int  width_,
                     unsigned int  height_,
                     GLint         internal_format_,
                     GLenum        format,
                     GLenum        type,
                     GLint         min_filter,
                     GLint         max_filter,
                     GLint         wrap_r,
                     GLint         wrap_s,
                     const GLvoid* data)
    : m_Width(width_)
    , m_Height(height_)
    , m_InternalFormat(internal_format_)
{
    glGenTextures(1, &m_ID);

    Bind();

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, max_filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, wrap_r);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_s);

    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 m_InternalFormat,
                 m_Width,
                 m_Height,
                 0,
                 format,
                 type,
                 data);

    Unbind();
}

Texture2D::~Texture2D()
{
    glDeleteTextures(1, &m_ID);
}

Texture2D* Texture2D::CreateFromFile(const char* path, bool flip_vertically)
{
    Texture2D* result = nullptr;

    stbi_set_flip_vertically_on_load(flip_vertically);

    int      width, height, channel_count;
    stbi_uc* data = stbi_load(path, &width, &height, &channel_count, 0);

    if (data)
    {
        GLint  internal_format(-1);
        GLenum format(-1);

        switch (channel_count)
        {
            case 1:
            {
                internal_format = GL_R8;
                format          = GL_RED;
            }
            break;

            case 3:
            {
                internal_format = GL_RGB8;
                format          = GL_RGB;
            }
            break;

            case 4:
            {
                internal_format = GL_RGBA8;
                format          = GL_RGBA;
            }
            break;

            default:
            {
                std::cout << "File format not supported yet!" << std::endl;
            }
            break;
        }

        result = new Texture2D(width,
                               height,
                               internal_format,
                               format,
                               GL_UNSIGNED_BYTE,
                               GL_NEAREST,
                               GL_NEAREST,
                               GL_CLAMP_TO_BORDER,
                               GL_CLAMP_TO_BORDER,
                               data);
    }
    else
    {
        std::cout << "Failed to load image at path: " << path << std::endl;
    }

    stbi_image_free(data);

    return result;
}

void Texture2D::SetWrappingParams(GLint wrap_r, GLint wrap_s)
{
    glTexParameteri(m_ID, GL_TEXTURE_WRAP_R, wrap_r);
    glTexParameteri(m_ID, GL_TEXTURE_WRAP_S, wrap_s);
}

void Texture2D::BindImage(GLuint unit, GLenum access, GLenum format) const
{
    glBindImageTexture(unit, m_ID, 0, GL_FALSE, 0, access, format);
}
