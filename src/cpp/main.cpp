#include "glad.h"

#include "volumetricclouds.hpp"


int main()
{
    VolumetricClouds app;
    return app.Run();
}
