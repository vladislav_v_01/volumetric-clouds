#include "glad.h"

#include "volumetricclouds.hpp"

#include <iostream>


#include "singleton.h"

#include "appsettings.hpp"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

VolumetricClouds::VolumetricClouds()
{

}

VolumetricClouds::~VolumetricClouds()
{

}

void VolumetricClouds::Initialize()
{
    m_SkyShader = std::make_unique<Shader>("../../src/shaders/screen.vert",
                                           "../../src/shaders/sky.frag");

    float        vertices[] = { -1.0f, -1.0f, 0.0, 0.0, 1.0f, -1.0f, 1.0, 0.0,
                         -1.0f, 1.0f,  0.0, 1.0, 1.0f, 1.0f,  1.0, 1.0,
                         -1.0f, 1.0f,  0.0, 1.0, 1.0f, -1.0f, 1.0, 0.0 };

    glGenVertexArrays(1, &m_QuadVAO);
    glGenBuffers(1, &m_QuadVBO);
    glBindVertexArray(m_QuadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, m_QuadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(
        0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1,
                          2,
                          GL_FLOAT,
                          GL_FALSE,
                          4 * sizeof(float),
                          (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);
}

void VolumetricClouds::Update()
{
    if (this->m_ShowDebugGui)
    {
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
                    1000.0f / ImGui::GetIO().Framerate,
                    ImGui::GetIO().Framerate);
        ImGui::InputFloat("Angle of sun", &m_AngleOfSun, 1.0f, 10.0f);
        ImGui::InputFloat("Earth Radius", &m_EarthRadius, 1.0f, 10.0f);
        ImGui::InputFloat("Atmoshere Radius", &m_AtmodhereRadius, 1.0f, 10.0f);
        ImGui::InputFloat("Hr", &m_Hr, 0.1f, 1.0f);
        ImGui::InputFloat("Hm", &m_Hm, 0.1f, 1.0f);
        ImGui::InputFloat3("BetaR", &m_BetaR[0], "%0.8f");
        ImGui::InputFloat3("BetaM", &m_BetaM[0], "%0.8f");
        ImGui::InputInt("Number of samples", &m_NumSamples, 1, 10);
        ImGui::InputInt("Number of samples light", &m_NumSamplesLight, 1, 10);
    }

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    m_SkyShader->use();
    float angle = m_AngleOfSun / float(128 - 1) * M_PI * 0.6;
    m_SunDirection = glm::vec3(0, std::cos(angle), -std::sin(angle));
    m_SkyShader->setVec3("sunDirection", m_SunDirection);
    m_SkyShader->setFloat("earthRadius", m_EarthRadius);
    m_SkyShader->setFloat("atmosphereRadius", m_AtmodhereRadius);
    m_SkyShader->setFloat("Hr", m_Hr);
    m_SkyShader->setFloat("Hm", m_Hr);
    m_SkyShader->setVec3("betaR", m_BetaR);
    m_SkyShader->setVec3("betaM", m_BetaM);
    m_SkyShader->setInt("numSamples", m_NumSamples);
    m_SkyShader->setInt("numSamplesLight", m_NumSamplesLight);
    m_SkyShader->setFloat("aspectRatio",
                       static_cast<float>(SINGLETON(AppSettings)->get_Width()) /
                           static_cast<float>(SINGLETON(AppSettings)->get_Height()));
    m_SkyShader->setVec2("resolution", glm::vec2(SINGLETON(AppSettings)->get_Width(), SINGLETON(AppSettings)->get_Height()));

    glBindVertexArray(m_QuadVAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

int VolumetricClouds::Run()
{
    this->InitializeBase();

    this->Initialize();

    while (!glfwWindowShouldClose(this->m_Window))
    {
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        float start_time = (float)glfwGetTime();

        this->Update();

        this->m_DeltaTime = (float)glfwGetTime() - start_time;

        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(this->m_Window);
        glfwPollEvents();
    }

    return 0;

}

void VolumetricClouds::InitializeBase()
{
    if (glfwInit() != GLFW_TRUE)
    {
        exit(-1);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    glfwWindowHint(GLFW_RESIZABLE, true);

    // create GLFWwindow
    this->m_Window = glfwCreateWindow(SINGLETON(AppSettings)->get_Width(),
                                SINGLETON(AppSettings)->get_Height(),
                                SINGLETON(AppSettings)->get_WindowTitle().c_str(),
                                nullptr,
                                nullptr);
    if (this->m_Window == nullptr)
    {
        glfwTerminate();
        exit(-1);
    }
    glfwMakeContextCurrent(this->m_Window);

    // switch on callbacks
    glfwSetFramebufferSizeCallback(this->m_Window, this->GLFWFramebufferSizeCallbackHelper);
    glfwSetKeyCallback(this->m_Window, this->GLFWKeyCallbackHelper);
    glfwSetCursorPosCallback(this->m_Window, this->GLFWMouseCallbackHelper);
    glfwSetScrollCallback(this->m_Window, this->GLFWScrollCallbackHelper);

    // on/off cursor
    glfwSetInputMode(this->m_Window,
                     GLFW_CURSOR,
                     SINGLETON(AppSettings)->get_EnableCursor() ? GLFW_CURSOR_NORMAL
                                                                : GLFW_CURSOR_DISABLED);
    glfwSetWindowUserPointer(this->m_Window, this);
    glfwMakeContextCurrent(this->m_Window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cerr << "Failed to initialize GLAD" << std::endl;
        exit(-1);
    }

    // enable OpenGL debug context if context allows for debug context
    int flags;
    glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
    if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
    {
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS); // makes sure errors are
                                               // displayed synchronously
        glDebugMessageCallback(&AppSettings::glDebugOutput, nullptr);
        glDebugMessageControl(
            GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
    }

    // initialize ImGui
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    ImGui_ImplGlfw_InitForOpenGL(this->m_Window, true);
    ImGui_ImplOpenGL3_Init();

    m_LastX = SINGLETON(AppSettings)->get_Width() / 2.f;
    m_LastY = SINGLETON(AppSettings)->get_Height() / 2.f;
}


void VolumetricClouds::GLFWFramebufferSizeCallbackHelper(GLFWwindow* window,
                                              int         width,
                                              int         height)
{
    VolumetricClouds* app = (VolumetricClouds*)glfwGetWindowUserPointer(window);
    app->GLFWFramebufferSizeCallback(window, width, height);
}

void VolumetricClouds::GLFWMouseCallbackHelper(GLFWwindow* window,
                                    double      x_pos,
                                    double      y_pos)
{
    VolumetricClouds* app = (VolumetricClouds*)glfwGetWindowUserPointer(window);
    app->GLFWMouseCallback(window, x_pos, y_pos);
}

void VolumetricClouds::GLFWScrollCallbackHelper(GLFWwindow* window,
                                     double      x_offset,
                                     double      y_offset)
{
    VolumetricClouds* app = (VolumetricClouds*)glfwGetWindowUserPointer(window);
    app->GLFWScrollCallback(window, x_offset, y_offset);
}

void VolumetricClouds::GLFWKeyCallbackHelper(
    GLFWwindow* window, int key, int scancode, int action, int mods)
{
    VolumetricClouds* app = (VolumetricClouds*)glfwGetWindowUserPointer(window);
    app->GLFWKeyCallback(window, key, scancode, action, mods);
}

void VolumetricClouds::GLFWFramebufferSizeCallback(GLFWwindow* window,
                                        const int   width,
                                        const int   height)
{
    glViewport(0, 0, width, height);
    OnWindowResize(width, height);
}

void VolumetricClouds::GLFWMouseCallback(GLFWwindow*  window,
                              const double x,
                              const double y)
{
    OnMouseMove(float(x - m_LastX), float(m_LastY - y));
    m_LastX = (float)x;
    m_LastY = (float)y;
}

void VolumetricClouds::GLFWScrollCallback(GLFWwindow*  window,
                               const double x_offset,
                               const double y_offset)
{
    OnMouseScroll((float)y_offset);
}

void VolumetricClouds::GLFWKeyCallback(GLFWwindow* window,
                            const int   key,
                            const int   scancode,
                            const int   action,
                            const int   mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

    if (action == GLFW_PRESS || action == GLFW_REPEAT)
        OnKeyPress(key);
    else if (action == GLFW_RELEASE)
        OnKeyRelease(key);
}

void VolumetricClouds::OnWindowResize(const int width, const int height) {
    SINGLETON(AppSettings)->set_Width(width);
    SINGLETON(AppSettings)->set_Height(height);
}

void VolumetricClouds::OnKeyPress(int key_code)
{
    // Todo:
    // 1. I would like the client to not touch GLFW code.
    // 2. This destroys the coherence between settings.enable_cursor and the
    // actual state of the cursor, fix this.

    if (key_code == GLFW_KEY_G)
    {
        if (!this->m_ShowDebugGui)
        {
            glfwSetInputMode(this->m_Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            this->m_ShowDebugGui = true;
        }
        else
        {
            glfwSetInputMode(this->m_Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            this->m_ShowDebugGui = false;
        }
    }

//    if (!m_ShowDebugGui)
//        this->m_UserCamera->ProcessKeyboard(key_code, this->m_DeltaTime);
}

void VolumetricClouds::OnKeyRelease(const int keyCode) {}

void VolumetricClouds::OnMouseMove(float x_offset, float y_offset)
{
//    if (!m_ShowDebugGui)
//        this->m_UserCamera->ProcessMouseMove(x_offset, y_offset);
}

void VolumetricClouds::OnMouseScroll(const float verticalOffset)
{
//    if (!m_ShowDebugGui)
//        this->m_UserCamera->ProcessMouseScroll(verticalOffset);
}
